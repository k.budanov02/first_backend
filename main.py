from flask import Flask

app = Flask(__name__)

a = {'info': 'Kolya'}


@app.route('/')
def hello_world():
    # print('haha')
    # for i in ['1', 2]:
    #     print(i)
    return 'My first backend'


@app.route('/info')
def info():
    # print('haha')
    # for i in ['1', 2]:
    #     print(i)
    return 'Some info'


@app.route('/developer_info')
def dev_info():
    # print('haha')
    # for i in ['1', 2]:
    #     print(i)
    return a['info']


database = {1: {'first_name': 'Oleg',
                'last_name': 'Petrov',
                'age': '100',
                },
            2: {'first_name': 'Ivan',
                'last_name': 'Gorin',
                'age': '9',
                }}


# /profile/1
# /profile/2
# /profile/22133
# /profile/8782
@app.route('/profile/<int:profile_id>')
def profile(profile_id):
    # show the post with the given id, the id is an integer
    first_name = database[profile_id]['first_name']
    last_name = database[profile_id]['last_name']
    age = database[profile_id]['age']
    return f'Firsname: {first_name}\nLastname: {last_name}\nAge: {age} '
